<?php
namespace Db\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="acl_resource", uniqueConstraints={@UniqueConstraint(columns={"module", "controller", "action"})})
 *
 */
class ACLResource
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $module;


    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $controller;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $action;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $resourceName;


    /**
     * @var ACLResource
     * @ORM\ManyToOne(targetEntity="Db\Entity\ACLResource")
     * @ORM\OrderBy({"sort_order" = "ASC"})
     */
    protected $parent;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     */
    public function setController($controller)
    {
        $this->controller = $controller;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getResourceName()
    {
        return $this->resourceName;
    }

    /**
     * @param string $resourceName
     */
    public function setResourceName($resourceName)
    {
        $this->resourceName = $resourceName;
    }

    /**
     * @return ACLResource
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param ACLResource $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }


}
