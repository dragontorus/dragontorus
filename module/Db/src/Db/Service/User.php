<?php

namespace Db\Service;


use Db\DBAL\Type\UserStatus;
use Zend\Crypt\Password\Bcrypt;
use Db\Entity\User as UserEntity;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class User
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * User constructor.
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Creates a new user
     *
     * @param array $data
     * @param array $roles
     * @return UserEntity
     */
    public function createUser($data, $roles = ['user']){
        $hydrator = new DoctrineHydrator($this->getEntityManager());
        $userEntity = new UserEntity();
        $hydrator->hydrate($data, $userEntity);

        $userEntity->setSalt($this->generateSalt());
        $userEntity->setPassword(self::hashPassword($userEntity, $userEntity->getPassword()));
        $userEntity->setStatus(UserStatus::STATUS_DISABLED);

        foreach ($roles as $roleName) {
            $role = $this->getEntityManager()->getRepository('Db\Entity\Role')->findOneBy(['roleId' => $roleName]);
            $userEntity->getRoles()->add($role);
        }

        $this->getEntityManager()->persist($userEntity);
        $this->getEntityManager()->flush($userEntity);

        return $userEntity;
    }

    /**
     * Creates or updates a user
     *
     * @param $data
     * @return UserEntity
     */
    public function saveUser($data){
        $em = $this->getEntityManager();
        $hydrator = new DoctrineHydrator($this->getEntityManager());

        if(isset($data['id']) && !empty($data['id'])) {
            $user = $em->getRepository('Db\Entity\User')->findOneBy(['id' => $data['id']]);
        } else {
            $user = new UserEntity();
        }

        if(empty($data['password'])){
            unset($data['password']);
        }

        $hydrator->hydrate($data, $user);

        if(!empty($data['password'])){
            $user->setSalt($this->generateSalt());
            $user->setPassword(self::hashPassword($user, $user->getPassword()));
        }

        $em->persist($user);
        $em->flush();

        return $user;
    }

    /**
     * Generates random hash for salt
     *
     * @return string
     */
    public static function generateSalt(){
        return bin2hex(openssl_random_pseudo_bytes(22));
    }

    /**
     * Generates password hash using plane password and user's salt
     *
     * @param UserEntity $user
     * @param $password
     * @return string
     */
    public static function hashPassword(UserEntity $user, $password){
        $bcrypt = new Bcrypt(['salt' => $user->getSalt()]);
        return $bcrypt->create($password);
    }

    public function encryptPassword($password, $salt){
        $bcrypt = new Bcrypt(['salt' => $salt]);
        return $bcrypt->create($password);
    }

    /**
     * Checks if test password hash matches with user password hash and if user is not disabled
     *
     * @param UserEntity $user
     * @param $password
     * @return bool
     */
    public static function checkUserCanLogin(UserEntity $user, $password){
        return $user->getStatus() == UserStatus::STATUS_ACTIVE && $user->getPassword() === self::hashPassword($user, $password);
    }

    /**
     * Get list of users
     *
     * @param array $filter
     * @param array $orderBy
     * @return UserEntity[]
     */
    public function listUserEntities($filter = [], $orderBy = []){
        return $this->getEntityManager()->getRepository('Db\Entity\User')->findBy($filter, $orderBy);
    }

    public function listUsersForDatatables($filter = [], $orderBy = []){
        $hydrator = new DoctrineHydrator($this->getEntityManager());

        $userEntityList = $this->listUserEntities($filter, $orderBy);

        $list = [];

        /**
         * @var $user UserEntity
         */
        foreach($userEntityList as $user){
            $row = $hydrator->extract($user);

            $row['roles'] = $user->getRoles()->map(function($role){ return $role->getRoleId(); })->toArray();
            $row['roles'] = implode(', ', $row['roles']);

            $row['createdAt'] = $row['createdAt']->format('Y-m-d H:i:s');
            $row['updatedAt'] = $row['updatedAt']->format('Y-m-d H:i:s');

            unset($row['salt'], $row['password']);

            $list[] = $row;
        }

        return $list;
    }

    /**
     * Get user by id
     *
     * @param $id
     * @return null|UserEntity
     */
    public function getUserById($id){
        return $this->getEntityManager()->getRepository('Db\Entity\User')->findOneBy(['id' => $id]);
    }

    /**
     * Get user by email
     *
     * @param $id
     * @return null|UserEntity
     */
    public function getUserByEmail($email){
        return $this->getEntityManager()->getRepository('Db\Entity\User')->findOneBy(['email' => $email]);
    }

    /**
     * Removes a list of users by given ids
     *
     * @param $userIds
     * @throws \Exception
     */
    public function removeUsers($userIds)
    {
        $em = $this->getEntityManager();

        $userRepository = $em->getRepository('Db\Entity\User');

        try {
            $em->beginTransaction();
            /**
             * @var $userList UserEntity[]
             */
            $userList = $userRepository->findBy(['id' => $userIds]);

            foreach($userList as $user){
                $em->remove($user);
            }

            $em->flush();
            $em->commit();
        } catch(\Exception $e){
            $em->rollback();
            throw $e;
        }
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }
}