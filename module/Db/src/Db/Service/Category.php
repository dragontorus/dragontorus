<?php

namespace Db\Service;


use Db\Entity\Category as CategoryEntity;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class Category
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * User constructor.
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Creates a new category
     *
     * @param array $data
     * @return CategoryEntity
     */
    public function createCategory($data){

        $hydrator = new DoctrineHydrator($this->getEntityManager());
        $categoryEntity = new CategoryEntity();


        $hydrator->hydrate($data, $categoryEntity);
        
        $this->getEntityManager()->persist($categoryEntity);
        $this->getEntityManager()->flush($categoryEntity);

        return $categoryEntity;
    }

    /**
     * Update a category
     *
     * @param $data
     * @param $categoryId
     * @return CategoryEntity|null
     * @throws \Exception
     */
    public function updateCategory($data, $categoryId){
        $em = $this->getEntityManager();
        $hydrator = new DoctrineHydrator($this->getEntityManager());

        if(!$category = $this->getCategoryById($categoryId)){
            throw new \Exception('Category not found', 404);
        }

        $hydrator->hydrate($data, $category);


        $em->persist($category);
        $em->flush();

        return $category;
    }

    /**
     * Get all categories
     *
     * @param Array $params
     * @return null|CategoryEntity
     */
    public function getAllCategories($params){
        if(isset($params['order'])&&!empty($params['order'])){
            if(isset($params['field'])&&!empty($params['field'])){
                return $this->getEntityManager()->getRepository('Db\Entity\Category')->findBy([],[$params['field']=>$params['order']]);
            } else {
                return $this->getEntityManager()->getRepository('Db\Entity\Category')->findBy([],['name'=>$params['order']]);
            }
        } else {
            if(isset($params['field'])&&!empty($params['field'])){
                return $this->getEntityManager()->getRepository('Db\Entity\Category')->findBy([],[$params['field']=>'ASC']);
            } else {
                return $this->getEntityManager()->getRepository('Db\Entity\Category')->findBy([],['name'=>'ASC']);
            }
        }
    }

    /**
     * Get category by id
     *
     * @param $id
     * @return null|CategoryEntity
     * @throws \Exception
     */
    public function getCategoryById($id){
        $category = $this->getEntityManager()->getRepository('Db\Entity\Category')->findOneBy(['id' => $id]);
        if(!$category){
            throw new \Exception('Category not found.',404);
        }
        return $category;
    }

    /**
     * Get category by name
     *
     * @param $name
     * @return null|CategoryEntity
     * @throws \Exception
     */
    public function getCategoryByName($name){
        $category =  $this->getEntityManager()->getRepository('Db\Entity\Category')->findBy(['name' => $name]);
        if(!$category){
            throw new \Exception('Category not found.',404);
        }
        return $category;
    }

    /**
     * Removes a list of categories by given ids
     *
     * @param $categoryIds
     * @throws \Exception
     */
    public function removeCategories($categoryIds)
    {
        $em = $this->getEntityManager();

        try {
            $em->beginTransaction();
            /**
             * @var $categories CategoryEntity[]
             */
            $categories = $em->getRepository('Db\Entity\Category')->findBy(['id' => $categoryIds]);
            if(count($categories)==0){
                throw new \Exception('Not found any category.',404);
            }
            foreach($categories as $category){
                $em->remove($category);
            }

            $em->flush();
            $em->commit();
        } catch(\Exception $e){
            $em->rollback();
            throw $e;
        }
    }

    /**
     * Delete category
     *
     * @param $categoryId
     * @return bool
     * @throws \Exception
     */
    public function deleteCategory($categoryId){
        $em = $this->getEntityManager();

        try {
            $em->beginTransaction();
            /**
             * @var $categories CategoryEntity[]
             */
            $category = $em->getRepository('Db\Entity\Category')->findOneBy(['id' => $categoryId]);
            if(!$category){
                throw new \Exception('Category not found.',404);
            }

            $em->remove($category);

            $em->flush();
            $em->commit();
            return true;
        } catch(\Exception $e){
            $em->rollback();
            throw $e;
        }
    }


    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }
}