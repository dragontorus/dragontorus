<?php

namespace Db\Service;


use Db\Entity\Product as ProductEntity;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class Product
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * User constructor.
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Creates a new product
     *
     * @param $data
     * @return ProductEntity
     */
    public function createProduct($data){
        $hydrator = new DoctrineHydrator($this->getEntityManager());
        $productEntity = new ProductEntity();
        if(isset($data['categories'])&&!empty($data['categories'])){
            $categoryIds = explode(',',$data['categories']);
            unset($data['categories']);
            $products = $this->getEntityManager()->getRepository('Db\Entity\Category')->findBy(['id' => $categoryIds]);
            $productEntity->addCategories($products);
        }
        $hydrator->hydrate($data, $productEntity);


        $this->getEntityManager()->persist($productEntity);
        $this->getEntityManager()->flush($productEntity);

        return $productEntity;
    }

    /**
     * Update a product
     *
     * @param $data
     * @param $productId
     * @return ProductEntity
     * @throws \Exception
     */
    public function updateProduct($data, $productId){
        $em = $this->getEntityManager();
        $hydrator = new DoctrineHydrator($this->getEntityManager());

        $product = $this->getProductById($productId);
        if(!$product){
            throw new \Exception('Product not found',404);
        }
        $hydrator->hydrate($data, $product);


        $em->persist($product);
        $em->flush();

        return $product;
    }

    /**
     * Get all products
     *
     * @param $params
     * @return array
     */
    public function getAllProducts($params){
        if(isset($params['order'])&&!empty($params['order'])){
            if(isset($params['field'])&&!empty($params['field'])){
                return $this->getEntityManager()->getRepository('Db\Entity\Product')->findBy([],[$params['field']=>$params['order']]);
            } else {
                return $this->getEntityManager()->getRepository('Db\Entity\Product')->findBy([],['name'=>$params['order']]);
            }
        } else {
            if(isset($params['field'])&&!empty($params['field'])){
                return $this->getEntityManager()->getRepository('Db\Entity\Product')->findBy([],[$params['field']=>'ASC']);
            } else {
                return $this->getEntityManager()->getRepository('Db\Entity\Product')->findBy([],['name'=>'ASC']);
            }
        }
    }

    /**
     * Get product by id
     *
     * @param $id
     * @return ProductEntity
     * @throws \Exception
     */
    public function getProductById($id){
        $product =  $this->getEntityManager()->getRepository('Db\Entity\Product')->findOneBy(['id' => $id]);
        if(!$product){
            throw new \Exception('Product not found',404);
        }

        return $product;

    }

    /**
     * Get product by name
     *
     * @param $name
     * @return ProductEntity
     * @throws \Exception
     */
    public function getProductByName($name){
        $product = $this->getEntityManager()->getRepository('Db\Entity\Product')->findBy(['name' => $name]);

        if(!$product){
            throw new \Exception('Product not found',404);
        }

        return $product;


    }

    /**
     * Removes a list of products by given ids
     *
     * @param $productIds
     * @throws \Exception
     */
    public function removeProducts($productIds)
    {
        $em = $this->getEntityManager();

        try {
            $em->beginTransaction();
            /**
             * @var $products ProductEntity[]
             */
            $products = $em->getRepository('Db\Entity\Product')->findBy(['id' => $productIds]);

            if(count($products)==0){
                throw new \Exception('Product not found',404);
            }



            foreach($products as $product){
                $em->remove($product);
            }

            $em->flush();
            $em->commit();
        } catch(\Exception $e){
            $em->rollback();
            throw $e;
        }
    }

    /**
     * Delete product
     *
     * @param $productId
     * @return bool
     * @throws \Exception
     */

    public function deleteProduct($productId){
        $em = $this->getEntityManager();

        try {
            $em->beginTransaction();
            /**
             * @var $categories CategoryEntity[]
             */
            $product = $em->getRepository('Db\Entity\Product')->findOneBy(['id' => $productId]);
            if(!$product){
                throw new \Exception('Product not found',404);
            }

            $em->remove($product);

            $em->flush();
            $em->commit();

            return true;
        } catch(\Exception $e){
            $em->rollback();
            throw $e;
        }
    }


    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }
}