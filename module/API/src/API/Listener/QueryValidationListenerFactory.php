<?php
namespace API\Listener;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
class QueryValidationListenerFactory implements FactoryInterface {
    /**
     * @param ServiceLocatorInterface $services
     * @return QueryValidationListener
     */
    public function createService(ServiceLocatorInterface $services)
    {
        $config = array();
        if ($services->has('Config')) {
            $allConfig = $services->get('Config');
            if (isset($allConfig['zf-content-validation'])) {
                $config = $allConfig['zf-content-validation'];
            }
        }
        return new QueryValidationListener($config, $services->get('InputFilterManager'));
    }
}