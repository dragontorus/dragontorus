<?php

namespace API\Listener;


use Db\Entity\Role;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\SharedListenerAggregateInterface;

class RoleRenderListener implements SharedListenerAggregateInterface
{

    protected $listeners = [];

    public function onRenderEntity($e)
    {
        /**
         * @var $entity \ZF\Hal\Entity
         */
        $entity = $e->getParam('entity');
        if (! $entity->entity instanceof Role) {
            return;
        }

        $payload = $e->getParam('payload');

        if(!$payload['parent']) {
            unset($payload['parent']);
        }

    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the SharedEventManager
     * implementation will pass this to the aggregate.
     *
     * @param SharedEventManagerInterface $events
     */
    public function attachShared(SharedEventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('ZF\Hal\Plugin\Hal', 'renderEntity.post', [$this, 'onRenderEntity']);
    }

    /**
     * Detach all previously attached listeners
     *
     * @param SharedEventManagerInterface $events
     */
    public function detachShared(SharedEventManagerInterface $events)
    {
        foreach($this->listeners as $listener){
            $events->detach($listener);
        }
    }


}