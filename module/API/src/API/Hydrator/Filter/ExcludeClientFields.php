<?php


namespace API\Hydrator\Filter;


use Zend\Stdlib\Hydrator\Filter\FilterInterface;

class ExcludeClientFields implements FilterInterface
{
    /**
     * Should return true, if the given filter
     * does not match
     *
     * @param string $property The name of the property
     * @return bool
     */
    public function filter($property)
    {
        return !in_array($property, ['secret', 'publicKey', 'accessToken', 'refreshToken', 'authorizationCode', 'jwt', 'jti', 'user']);
    }

}