<?php
namespace API;

use Zend\Mvc\MvcEvent;
use ZF\Apigility\Provider\ApigilityProviderInterface;


class Module implements ApigilityProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        $app = $e->getTarget();
        $services = $app->getServiceManager();
        $sharedEvents = $services->get('SharedEventManager');
        $sharedEvents->attachAggregate($services->get('role_render_listener'));
        $sharedEvents->attachAggregate($services->get('user_listener'));
        $sharedEvents->attachAggregate($services->get('API\\Listener\\QueryValidationListener'));
    }
}
