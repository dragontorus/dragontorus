<?php
namespace API\V1\Rest\User;


use ZF\Apigility\Doctrine\Server\Resource\DoctrineResource;

class UserResource extends DoctrineResource
{
    /**
     * Fetch a resource
     *
     * If the extractCollections array contains a collection for this resource
     * expand that collection instead of returning a link to the collection
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        if(!is_numeric($id)) {
            $authIdentity = $this->getIdentity()->getAuthenticationIdentity();
            if(isset($authIdentity['user_id'])) {
                $id = $authIdentity['user_id'];
            }
        }

        return parent::fetch($id);
    }

}
